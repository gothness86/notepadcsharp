﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotepadCSharp
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        // Переход по ссылке
        private void VisitLink()
        {
            linkLabel.LinkVisited = true;
            System.Diagnostics.Process.Start("https://bitbucket.org/gothness86/");
        }

        private void linkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try { VisitLink(); }
            catch (Exception ex)
            {
                MessageBox.Show(ex + "Unable to open link that was clicked.");
            }
        }

        // Закрытие окна
        private void button_Click(object sender, EventArgs e) { Close(); }
    }
}
