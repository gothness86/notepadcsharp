﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotepadCSharp
{
    public partial class MainWindow : Form
    {

        private int openDocuments = 0;
        
        // Элементы меню доступные для открытых документов
        private ToolStripMenuItem[] documentMenuItems;

        // Элементы панели инструментов для открытых документов
        private ToolStripButton[] documentToolItems;


        public MainWindow()
        {
            InitializeComponent();

            documentMenuItems = new ToolStripMenuItem[] {
                menuSave, menuSaveAs,
                menuCopy, menuCut, menuPaste, menuDelete, menuSelectAll, menuFind,
                menuFont, menuColor
            };

            documentToolItems = new ToolStripButton[] {
                toolSave, toolCopy, toolCut, toolPaste,
            };
            
            // Отключаем элементы меню для документа
            foreach (ToolStripMenuItem item in documentMenuItems) { item.Enabled = false; }

            // Отключаем элементы панели инструментов
            foreach (ToolStripButton item in documentToolItems) { item.Enabled = false; }

        }


        // Создание документа
        private Blank newDocument()
        {
            Blank form = new Blank();

            form.MdiParent = this;

            // Включаем элементы меню для документа
            foreach (ToolStripMenuItem item in documentMenuItems) { item.Enabled = true; }

            // Включаем элементы панели инструментов
            foreach (ToolStripButton item in documentToolItems) { item.Enabled = true; }

            // Имя документа по умолчанию
            form.DocName = "Untitled " + ++openDocuments;

            // Добавляем обработчик закрытия окна
            form.FormClosed += (sender, e) => { closeDocument(); };

            form.Show();

            return form;
        }


        // Закрытие документа
        private void closeDocument()
        {
            if (MdiChildren.Length > 1) { return; }

            // Отключаем элементы меню для документа
            foreach (ToolStripMenuItem item in documentMenuItems) { item.Enabled = false; }

            // Отключаем элементы панели инструментов
            foreach (ToolStripButton item in documentToolItems) { item.Enabled = false; }
        }


        // Получение активного документа
        private Blank getDocument()
        {
            // Создаём исключение если нет активных окон
            if (MdiChildren.Length == 0)
            {
                throw new InvalidOperationException("Document is unvalible");
            }

            return (Blank)this.ActiveMdiChild;
        }


        // Создание документа
        private void menuNew_Click(object sender, EventArgs e) { newDocument(); }


        // Открытие файла
        private void menuOpen_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Blank form = newDocument();
                form.Open(openFileDialog1.FileName);
            }

        }


        // Сохранение файла
        private void menuSave_Click(object sender, EventArgs e)
        {
            Blank form = getDocument();

            if (form.FilePath == "")
            {
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    form.Save(saveFileDialog1.FileName);
                }
            }
            else { form.Save(form.FilePath); }
        }


        // Сохранить файл как
        private void menuSaveAs_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                getDocument().Save(saveFileDialog1.FileName);
            }
        }


        // Выход
        private void menuQuit_Click(object sender, EventArgs e) { Close(); }


        // Операции над текстом
        private void menuCopy_Click(object sender, EventArgs e) { getDocument().Copy(); }
        private void menuCut_Click(object sender, EventArgs e) { getDocument().Cut(); }
        private void menuPaste_Click(object sender, EventArgs e) { getDocument().Paste(); }
        private void menuDelete_Click(object sender, EventArgs e) { getDocument().Delete(); }
        private void menuSelectAll_Click(object sender, EventArgs e) { getDocument().SelectAll(); }


        // Поиск
        private void menuFind_Click(object sender, EventArgs e)
        {
            FindForm form = new FindForm();
            if (form.ShowDialog() == DialogResult.OK)
            {
                getDocument().Find(form.FindText, form.FindCondition);
            }
        }


        // Расположение окон
        private void menuArrangeIcons_Click(object sender, EventArgs e) { LayoutMdi(MdiLayout.ArrangeIcons); }
        private void menuCascade_Click(object sender, EventArgs e) { LayoutMdi(MdiLayout.Cascade); }
        private void menuTileHorizontal_Click(object sender, EventArgs e) { LayoutMdi(MdiLayout.TileHorizontal); }
        private void menuTileVertical_Click(object sender, EventArgs e) { LayoutMdi(MdiLayout.TileVertical); }


        // Выбор шрифта
        private void menuFont_Click(object sender, EventArgs e)
        {
            Blank form = getDocument();

            fontDialog1.Font = form.SelectionFont;
            fontDialog1.Color = form.SelectionColor;

            if (fontDialog1.ShowDialog() == DialogResult.OK)
            {
                form.SelectionFont = fontDialog1.Font;
                form.SelectionColor = fontDialog1.Color;
            }
        }


        // Выбор цвета
        private void menuColor_Click(object sender, EventArgs e)
        {
            Blank form = getDocument();

            colorDialog1.Color = form.SelectionColor;

            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                form.SelectionColor = colorDialog1.Color;
            }
        }


        // О программе
        private void menuAboutProgram_Click(object sender, EventArgs e)
        {
            About form = new About();
            form.ShowDialog();
        }

        // Панель инструментов
        private void toolNew_Click(object sender, EventArgs e) { menuNew_Click(sender, e); }
        private void toolOpen_Click(object sender, EventArgs e) { menuOpen_Click(sender, e); }
        private void toolSave_Click(object sender, EventArgs e) { menuSave_Click(sender, e); }
        private void toolCopy_Click(object sender, EventArgs e) { menuCopy_Click(sender, e); }
        private void toolCut_Click(object sender, EventArgs e) { menuCut_Click(sender, e); }
        private void toolPaste_Click(object sender, EventArgs e) { menuPaste_Click(sender, e); }
    }
}
