﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotepadCSharp
{
    public partial class FindForm : Form
    {

        // Текстовое поле
        public string FindText
        {
            get { return findText.Text; }
        }

        // Настройка поиска
        public RichTextBoxFinds FindCondition
        {
            get
            {
                if (cbMatchCase.Checked && cbMatchWhole.Checked)
                {
                    return RichTextBoxFinds.MatchCase | RichTextBoxFinds.WholeWord;
                }

                if (cbMatchCase.Checked) { return RichTextBoxFinds.MatchCase; }
                if (cbMatchWhole.Checked) { return RichTextBoxFinds.WholeWord; }
                
                return RichTextBoxFinds.None;
            }
        }

        public FindForm()
        {
            InitializeComponent();
        }

        // Выполнить поиск по нажатию на Enter на поле ввода
        private void textFind_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }
    }
}
