﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotepadCSharp
{
    public partial class Blank : Form
    {
        private bool isSaved = true;
        private string filePath = "";

        // Буфер для выполнения операций с текстом
        private string bufferString = "";

        // Заголовок документа
        public string DocName
        {
            get { return Text; }
            set { Text = value; }
        }

        // Статус
        public bool IsSaved
        {
            get { return isSaved; }
        }

        // Путь к сохранённому документу
        public string FilePath
        {
            get { return filePath; }
        }

        // Шрифт текста
        public Font SelectionFont
        {
            get { return richTextBox.SelectionFont; }
            set { richTextBox.SelectionFont = value;  }
        }

        // Цвет текста
        public Color SelectionColor
        {
            get { return richTextBox.SelectionColor;  }
            set { richTextBox.SelectionColor = value;  }
        }

        public Blank()
        {
            InitializeComponent();

            // Устанавливаем начальное количество символов
            updateAmount();

            // Устанавливаем время открытия или создания документа
            sbTime.Text = DateTime.Now.ToShortTimeString();
            sbTime.ToolTipText = DateTime.Now.ToLongDateString();
        }

        // Открытие файла
        public void Open(string OpenFilePath)

        {
            if (OpenFilePath == "") { return; }

            using (StreamReader sr = new StreamReader(OpenFilePath))
            {
                richTextBox.Text = sr.ReadToEnd();
            };

            // Обновление счётчика символов
            updateAmount();

            DocName = filePath = OpenFilePath;
            isSaved = true;
        }

        // Сохранение файла
        public void Save(string SaveFilePath)

        {
            if (SaveFilePath == "") { return; }

            using (StreamWriter sw = new StreamWriter(SaveFilePath))
            {
                sw.Write(richTextBox.Text);
            };
            
            DocName = filePath = SaveFilePath;
            isSaved = true;
        }

        // Обновление строки с количестом сиволов
        private void updateAmount()
        {
            sbAmount.Text = "Amount of symbols: " + richTextBox.Text.Length.ToString();
        }

        // Операции над текстом
        public void Copy() { bufferString = richTextBox.SelectedText; }
        public void Cut()
        {
            bufferString = richTextBox.SelectedText;
            richTextBox.SelectedText = "";
        }

        public void Paste() { richTextBox.SelectedText = bufferString; }
        public void Delete() { richTextBox.SelectedText = ""; }
        public void SelectAll() { richTextBox.SelectAll(); }


        // Поиск
        public void Find(string value, RichTextBoxFinds condition)
        {
            richTextBox.Find(value, richTextBox.SelectionStart, condition);
        }
        
        private void menuCopy_Click(object sender, EventArgs e) { Copy(); }
        private void menuCut_Click(object sender, EventArgs e) { Cut(); }
        private void menuPaste_Click(object sender, EventArgs e) { Paste();  }
        private void menuDelete_Click(object sender, EventArgs e) { Delete(); }
        private void menuSelectAll_Click(object sender, EventArgs e) { SelectAll(); }

        // Событие на изменение текста
        private void richTextBox_TextChanged(object sender, EventArgs e)
        {
            updateAmount();
            isSaved = false;
        }

        // Закрытие документа
        private void Blank_FormClosed(object sender, FormClosedEventArgs e)
        {

            string message = "Do you want save changes in " + DocName + "?";

            // Если документ сохранён
            if (IsSaved == true) { return; }

            if (MessageBox.Show(message, "Message",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (FilePath == "")
                {
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        Save(saveFileDialog1.FileName);
                    }
                }
                else { Save(FilePath); }
            }

        }

    }
}
